import React from 'react'

import FormPage from './pages/FormPage'

function App() {
  return (
    <div className="container">
      <hr />
      <FormPage />
      <hr />
    </div>
  )
}

export default App
