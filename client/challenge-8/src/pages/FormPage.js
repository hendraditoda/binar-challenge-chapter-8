import React, { useState } from 'react';
import Input from '../components/Form/Input';

const FormPage = () => {
  const [tasks, setTasks] = useState([]);
  const [tasks1, setTasks1] = useState([]);
  const [tasks2, setTasks2] = useState([]);
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [experience, setExperience] = useState('');
  const [level, setLevel] = useState('');

  const addTask = () => {
    // if (username.trim() === '') return;
    setTasks([...tasks, username, email, experience, level]);
    setUsername('');
    setEmail('');
    setExperience('');
    setLevel('');
  };

  const editTask = () => {
    // if (username.trim() === '') return;
    setTasks1([...tasks, username, email, experience, level]);
    setUsername('');
    setEmail('');
    setExperience('');
    setLevel('');
  };

  const searchTask = () => {
    // if (username.trim() === '') return;
    setTasks2([...tasks, username, email, experience, level]);
    setUsername('');
    setEmail('');
    setExperience('');
    setLevel('');
  };

  function handleNameChange(e) {
    e.preventDefault();
    setUsername(e.target.value);
  }

  function handleEmailChange(e) {
    e.preventDefault();
    setEmail(e.target.value);
  }

  function handleExperienceChange(e) {
    e.preventDefault();
    setExperience(e.target.value);
  }

  function handleLevelChange(e) {
    e.preventDefault();
    setLevel(e.target.value);
  }

  return (
    <div>
      <Input name="name" value={username} handleOnChange={handleNameChange} placeholder="Input your username" />
      <Input name="email" value={email} handleOnChange={handleEmailChange} placeholder="Input your email" />
      <Input
        name="experience"
        type="number"
        value={experience}
        handleOnChange={handleExperienceChange}
        placeholder="Input your experience"
      />
      <Input
        name="level"
        type="number"
        value={level}
        handleOnChange={handleLevelChange}
        placeholder="Input your level"
      />
      <br />
      <button variant="primary" type="button" className="btn" onClick={addTask}>
        Add
      </button>
      <button variant="primary" type="button" className="btn" onClick={editTask}>
        Edit
      </button>
      <button variant="primary" type="button" className="btn" onClick={searchTask}>
        Search
      </button>
      <ul>
        {addTask &&
          tasks.map((task, index) => (
            <li key={index}>
              <h3>{task}</h3>
            </li>
          ))}
        {editTask &&
          tasks1.map((task, index) => (
            <li key={index}>
              <h3>{task}</h3>
            </li>
          ))}
        {searchTask &&
          tasks2.map((task, index) => (
            <li key={index}>
              <h3>{task}</h3>
            </li>
          ))}
      </ul>
    </div>
  );
};

export default FormPage;
